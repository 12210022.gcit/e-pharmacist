const mongoose = require('mongoose')
const bcrypt = require('bcryptjs')
const validator=require('validator')

const userSchema= new mongoose.Schema({
    name:{
        type: String,
        required: [true, 'Please tell ys your name!'],
    },
    email:{
        type: String,
        required: [true, 'please provide your email'],
        unique: true,
        lowercase: true,
        validate: [validator.isEmail, 'please provide a valid email'],
    },
    photo:{
        type: String,
        default: 'default.jpg'
    },
    role: {
        type:String,
        enum: ['user','sme','pharmacist', 'admin'],
        default: 'user',
    },
    password:{
        type: String,
        required: [true, 'Please provide a password!'],
        minleangth:8,
        // password wont be included when we get the users
        select: false,
    },
    passwordConfirm:{
        type: String,
        required:[true,'Please confirm your password'],

    validate:{
        validator: function(el){
            return el===this.password
        },
        message: 'Passwords are not the same'
    }},
    active:{
        type: Boolean,
        default: true,
        select: false,
    },
})

userSchema.pre('save', async function (next){
    // only run this function if password was actually modified
    if(!this.isModified('password')) return next()

    // Hash the password with cost of 12
    this.password = await bcrypt.hash(this.password, 12)
    // delete passwordConfirm field
    this.passwordConfirm = undefined
    next()
})
userSchema.pre('findOneAndupdate', async function (next){
    const update = this.getUpdate();
    if (update.password !== '' &&
    update.password !== undefined &&
    update.password == update.passwordConfirm){
    
        this.getUpdate().password = await bcrypt.hash(update.password, 12)
        update.passwordConfirm = undefinednext()
    }else
    next()
    }
)
userSchema.methods.correctPassword = async function(
    candidatePassword,
    userPassword,
){
    return await bcrypt.compare(candidatePassword, userPassword)
}
const User= mongoose.model('User', userSchema)
module.exports=User